package gps

import plateau.PlateauMap
import plateau.surface.Surface
import point.Point

interface Gps<T> {
    val plateauMap: PlateauMap
    fun findCurrentDirection(): T
    fun findNextDirectionWhenTurnRight(): T
    fun findNextDirectionWhenTurnLeft(): T
    fun isWantedPositionAccessible(wantedPosition: Point): Boolean
}