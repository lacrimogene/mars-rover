package gps

import plateau.MarsPlateauMap
import point.Point
import point.Point2D
import gps.RoverGps.Companion.Compass
import plateau.surface.Mountain

class RoverGps(
    private var currentOrientation: Int,
    override val plateauMap: MarsPlateauMap
) : Gps<Compass> {

    private val cardinalPoints = enumValues<Compass>()

    override fun findCurrentDirection(): Compass {
        return cardinalPoints[currentOrientation]
    }

    override fun findNextDirectionWhenTurnRight(): Compass {
        if (currentOrientation == Compass.W.ordinal)
            currentOrientation = Compass.N.ordinal
        else
            currentOrientation++
        return cardinalPoints[currentOrientation]
    }

    override fun findNextDirectionWhenTurnLeft(): Compass {
        if (currentOrientation == Compass.N.ordinal)
            currentOrientation = Compass.W.ordinal
        else
            currentOrientation--
        return cardinalPoints[currentOrientation]
    }

    override fun isWantedPositionAccessible(desiredPosition: Point): Boolean {
        return when (plateauMap.informTypeOfLandAtLocation(desiredPosition)) {
            is Mountain -> false
            else -> true
        }
    }

    companion object {
        enum class Compass(var point: Point2D) {
            N(Point2D(0, 1)),
            E(Point2D(1, 0)),
            S(Point2D(0, -1)),
            W(Point2D(-1, 0))
        }
    }
}
