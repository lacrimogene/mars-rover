import gps.RoverGps
import plateau.MarsPlateauMap
import point.Point2D
import route.MarsRouteInterpreter
import vehicle.Rover
import gps.RoverGps.Companion.Compass

fun main() {
    val rawRoute = arrayListOf("M", "M", "R", "M", "M", "R", "M", "R", "R", "M")
    val marsPlateauMap = MarsPlateauMap(boundaries = Point2D(5, 5))
    val controls = MarsRouteInterpreter().interpretRoute(rawRoute)

    val gps = RoverGps(
        plateauMap = marsPlateauMap,
        currentOrientation = Compass.E.ordinal
    )

    val rover = Rover(
        gps = gps,
        controls = controls
    )

    rover.landAt(position = Point2D(3, 3))
    rover.executeControls()
    print("${rover.position.x} ${rover.position.y} ${rover.direction.name}")
}