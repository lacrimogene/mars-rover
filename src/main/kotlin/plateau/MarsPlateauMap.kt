package plateau

import plateau.surface.Mountain
import plateau.surface.Surface
import point.Point

class MarsPlateauMap(
    override val boundaries: Point,
    override val surfaceTypes: HashMap<Int, Surface> = HashMap()
) : PlateauMap {

    fun MarsPlateauMap() {
        surfaceTypes[boundaries.x] = Mountain()
        surfaceTypes[boundaries.y] = Mountain()
    }

    override fun informTypeOfLandAtLocation(location: Point): Surface? {
        return surfaceTypes[location.x]
    }
}