package plateau

import plateau.surface.Surface
import point.Point

interface PlateauMap {
    val boundaries: Point
    val surfaceTypes: HashMap<Int, Surface>
    fun informTypeOfLandAtLocation(location: Point): Surface?
}