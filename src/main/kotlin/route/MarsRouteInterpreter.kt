package route

import vehicle.Rover.Companion.Controls

class MarsRouteInterpreter : RouteInterpreter<ArrayList<String>, ArrayList<Controls>> {
    companion object {
        const val RIGHT = "R"
        const val LEFT = "L"
        const val MOVE = "M"
    }

    override fun interpretRoute(route: ArrayList<String>): ArrayList<Controls> {
        val controls = ArrayList<Controls>()
        route.forEach {
            when (it) {
                LEFT -> controls.add(Controls.LEFT)
                RIGHT -> controls.add(Controls.RIGHT)
                MOVE -> controls.add(Controls.MOVE)
            }
        }
        return controls
    }
}