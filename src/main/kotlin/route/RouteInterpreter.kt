package route

interface RouteInterpreter<in T, out U> {
    fun interpretRoute(route: T): U
}