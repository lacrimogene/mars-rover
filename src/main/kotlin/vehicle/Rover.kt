package vehicle

import point.Point
import point.Point2D
import gps.RoverGps
import gps.RoverGps.Companion.Compass

class Rover(
    override var direction: Compass = Compass.N,
    override var position: Point = Point2D(0, 0),
    val gps: RoverGps,
    val controls: ArrayList<Controls>
) : Vehicle {

    override fun turnLeft() {
        direction = gps.findNextDirectionWhenTurnLeft()
    }

    override fun turnRight() {
        direction = gps.findNextDirectionWhenTurnRight()
    }

    override fun move() {
        val wantedPosition = Point2D(position.x + direction.point.x, position.y + direction.point.y)
        if (gps.isWantedPositionAccessible(wantedPosition)) {
            position = wantedPosition
        }
    }

    fun executeControls() {
        controls.forEach {
            when (it) {
                Controls.LEFT -> turnLeft()
                Controls.RIGHT -> turnRight()
                Controls.MOVE -> move()
            }
        }
    }

    fun landAt(position: Point2D) {
        this.position = position
        this.direction = gps.findCurrentDirection()
    }

    companion object {
        enum class Controls {
            LEFT,
            RIGHT,
            MOVE
        }
    }
}