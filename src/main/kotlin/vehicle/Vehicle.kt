package vehicle

import gps.RoverGps.Companion.Compass
import point.Point

interface Vehicle {
    var direction: Compass
    var position: Point
    fun move()
    fun turnLeft()
    fun turnRight()
}