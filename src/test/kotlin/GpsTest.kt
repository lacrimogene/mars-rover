import gps.RoverGps
import org.junit.*
import plateau.MarsPlateau
import point.Point2D

internal class GpsTest {
    @Test
    fun `should return West compass when called three times with RIGHT_ANGLE`() {
        val gps = RoverGps(Compass.N.ordinal)
        gps.findNextDirectionFrom(RIGHT_ANGLE)
        gps.findNextDirectionFrom(RIGHT_ANGLE)

        Assert.assertEquals(Compass.W, gps.findNextDirectionFrom(RIGHT_ANGLE))
    }

    @Test
    fun `should return East compass when called three times with LEFT_ANGLE`() {
        val gps = RoverGps(Compass.N.ordinal, MarsPlateau(Point2D(5, 5)))
        gps.findNextDirectionFrom(LEFT_ANGLE)
        gps.findNextDirectionFrom(LEFT_ANGLE)

        Assert.assertEquals(Compass.E, gps.findNextDirectionFrom(LEFT_ANGLE))
    }
}