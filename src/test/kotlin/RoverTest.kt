import gps.RoverGps
import org.junit.*
import point.Point2D
import vehicle.Rover

internal class RoverTest {
    @Test
    fun `Rover position should be, 3 1 when move is called with boundaries of 5 5 and direction is East`() {
        val rover = Rover(
            direction = Compass.N,
            position = Point2D(0, 1),
            gps = RoverGps(Compass.N.ordinal),
            plateau = marsPlateau
        )
        val boundaries = Point2D(3, 3)

        rover.turn(RIGHT)
        rover.move(boundaries)
        rover.move(boundaries)
        rover.move(boundaries)
        Assert.assertEquals(3, rover.position.x)
        Assert.assertEquals(1, rover.position.y)
    }

    @Test
    fun `Rover should stop at 0 4 when move is called with boundaries of 4 4 and direction is North`() {
        val rover = Rover(
            direction = Compass.N,
            position = Point2D(0, 1),
            gps = RoverGps(Compass.N.ordinal),
            plateau = marsPlateau
        )
        val boundaries = Point2D(4, 4)

        rover.move(boundaries)
        rover.move(boundaries)
        rover.move(boundaries)
        rover.move(boundaries)
        rover.move(boundaries)
        Assert.assertEquals(0, rover.position.x)
        Assert.assertEquals(4, rover.position.y)
    }

    @Test
    fun `Rover direction should face South when initial Direction is North and asked to turn Left two times`() {
        val rover = Rover(
            direction = Compass.N,
            position = Point2D(0, 1),
            gps = RoverGps(Compass.N.ordinal),
            plateau = marsPlateau
        )

        rover.turn(LEFT)
        rover.turn(LEFT)
        Assert.assertEquals(Compass.S.point.x, rover.direction.point.x)
        Assert.assertEquals(Compass.S.point.y, rover.direction.point.y)
    }

    @Test
    fun `Rover direction should face North when initial Direction is North and asked to turn Right then Left`() {
        val rover = Rover(
            direction = Compass.N,
            position = Point2D(0, 1),
            gps = RoverGps(Compass.N.ordinal),
            plateau = marsPlateau
        )

        rover.turn(RIGHT)
        rover.turn(LEFT)
        Assert.assertEquals(Compass.N.point.x, rover.direction.point.x)
        Assert.assertEquals(Compass.N.point.y, rover.direction.point.y)
    }
}